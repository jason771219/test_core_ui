class AppGlobal {
  	curUserInfo = {
      baseUrl:'', //request api 網址
      isLogin: false
	  };

    //換頁時傳遞參數用
    passValue = {
      expense_dict: {},
    };

  	constructor() {
      if (typeof window !== 'undefined') {
        var path = window.location.protocol + '//' + window.location.hostname; 
        var port = window.location.port
        port = '8000';//test target server port is 8000
        if (port !== 'undefined' && port !== '') {
          path = path + ':' + port
        }        

        this.curUserInfo.baseUrl = path
      }      
      console.log('AppGlobal constructor baseUrl:', this.curUserInfo.baseUrl);
  	}

    // localStorage : 永久儲存，除非指定移除 
    // sessionStorage : 瀏覽器關閉就移除資料
  	setDefault() {
  		this.curUserInfo.isLogin = false;
      sessionStorage.setItem('isLogin', this.curUserInfo.isLogin);
  	}

    //是否已登入
  	setIsLogin(isLogin) {
  		this.curUserInfo.isLogin = isLogin
      sessionStorage.setItem('isLogin', this.curUserInfo.isLogin);
  	}

    getIsLogin() {
      this.curUserInfo.isLogin = sessionStorage.getItem('isLogin');
      return this.curUserInfo.isLogin === 'true'
    }

    //Token
    setToken(token) {
      this.curUserInfo.token = token
      sessionStorage.setItem('token_face_sys', this.curUserInfo.token);
    }

    getToken() {
      this.curUserInfo.token = 'Token ' + sessionStorage.getItem('token_face_sys');
      return this.curUserInfo.token
    }

    //帳號
    setAccount(account) {
      this.curUserInfo.account = account
      sessionStorage.setItem('account_face_sys', this.curUserInfo.account);
    }

    getAccount() {
      this.curUserInfo.account = sessionStorage.getItem('account_face_sys');
      return this.curUserInfo.account
    }

        //預設帳號
    setDefaultUsername(default_username) {
      this.curUserInfo.default_username = default_username
      localStorage.setItem('default_username_face_sys', default_username);
    }

    getDefaultUsername() {
      this.curUserInfo.default_username = localStorage.getItem('default_username_face_sys');
      if (this.curUserInfo.default_username === null) {
        this.curUserInfo.default_username = ''
      };
      return this.curUserInfo.default_username
    }

    //預設密碼
    setDefaultPassword(default_password) {
      this.curUserInfo.default_password = default_password
      localStorage.setItem('default_password_face_sys', default_password);
    }

    getDefaultPassword() {
      this.curUserInfo.default_password = localStorage.getItem('default_password_face_sys');
      if (this.curUserInfo.default_password === null) {
        this.curUserInfo.default_password = ''
      };
      return this.curUserInfo.default_password
    }

    //後台位址
    getBaseUrl() {
      return this.curUserInfo.baseUrl
    }

  	logout(){
      console.log('logout')
    	this.setDefault();
  	}
}
export default (new AppGlobal());
