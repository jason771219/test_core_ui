import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';


import AppGlobal from '../../../app-global.js'

class Login extends Component {
  state = {
    save_alert_modal: false,
    error_alert_modal: false,
    username: "",
    password: "",
    error_message: '',
  };

  constructor(props) {
    super(props);
   
    this.toggleSaveAlertOK = this.toggleSaveAlertOK.bind(this);
    this.toggleSaveAlertNo = this.toggleSaveAlertNo.bind(this);
    this.toggleErrorAlert = this.toggleErrorAlert.bind(this);

    AppGlobal.logout();

    //平板上帳號密碼的自動存取，有時讀取會無法進入onChange作用，導致抓不到值，所以必須自行存取預設
    let username = AppGlobal.getDefaultUsername()
    let password = AppGlobal.getDefaultPassword()
    this.state = {
      save_alert_modal: false,
      error_alert_modal: false,
      account: username,
      password: password,
      error_message: '',
    };
  }

  async RouteChangeHome(){
    const path = '/';
    this.props.history.push(path);
  }

  async PostLogin(account, password) {
    //登入
    let error_message = ''
    try {

      const formData = new FormData()
      formData.append('account', account);
      formData.append('password', password);

      var baseUrl = AppGlobal.getBaseUrl()
      // const res = await fetch(baseUrl + '/base/v1/api/login/' + account + '/' + password + '/' + device_id + '/' + device_name + '/' , {
      //   method: 'GET',
      // });

      
      baseUrl = baseUrl + '/base/v1/api/login/'
      const res = await fetch(baseUrl, {
        method: 'POST',
        body: formData
      });

      if (res.status >= 200 && res.status < 300) {
        const data = await res.json();
       
        var status = data['status']
        var token = data['token']
        if (status === 0) {
            // LOGIN_SUCCESS
            AppGlobal.setIsLogin(true)
            AppGlobal.setAccount(account)
            AppGlobal.setToken(token)

            // 平板上帳號密碼的自動存取，有時讀取會無法進入onChange作用，導致抓不到值，所以必須自行存取預設
            AppGlobal.setDefaultUsername(account)
            AppGlobal.setDefaultPassword(password)
         
            this.RouteChangeHome()
        
        } else {
          // LOGIN_SUCCESS                      = 0
          // LOGIN_ACCOUNT_PASSWORD_EMPTY       = -1
          // LOGIN_ACCOUNT_PASSWORD_ERROR       = -2
          // LOGIN_EXCEPTION_ERROR              = -3
          // LOGIN_NO_PERMISSION                = -4          
          if (status === -1) {
            // LOGIN_ACCOUNT_PASSWORD_EMPTY
            error_message = '帳號或密碼為空'
          } else if (status === -2) {
            error_message = '帳號或密碼錯誤'
          } else {
            // LOGIN_EXCEPTION_ERROR
            error_message = '其他例外錯誤'            
          };
        };
      } else {
        var error = new Error(res.statusText)
        error.response = res
        // throw error
        console.log(res);
        
        error_message = '登入失敗'
      }
    } catch (e) {
      console.log(e);
      error_message = '登入失敗'
    } finally {
      if (error_message !== '') {
        this.setState({
          error_message: error_message,
          error_alert_modal: !this.state.error_alert_modal,
        });
      };
    }
  }


  async RouteChangeHome(){
    const path = '/';
    this.props.history.push(path);
  }


  toggleSaveAlert() {
    this.setState({
      save_alert_modal: !this.state.save_alert_modal,
    });
  }

  toggleSaveAlertOK() {
    this.toggleSaveAlert()

    let account = this.state.account
    if (account === '') {
      return
    };
    let password = this.state.password      
    if (password === '') {
      return
    };

    //平板上帳號密碼的自動存取，有時讀取會無法進入onChange作用，導致抓不到值，所以必須自行存取預設
    AppGlobal.setDefaultUsername(account)
    AppGlobal.setDefaultPassword(password)

    this.PostLogin(account, password)
  }

  toggleSaveAlertNo() {
    this.toggleSaveAlert()

    let account = this.state.account
    if (account === '') {
      return
    };
    let password = this.state.password      
    if (password === '') {
      return
    };

    this.PostLogin(account, password)
  }

  toggleErrorAlert() {
    this.setState({
      error_alert_modal: !this.state.error_alert_modal,
    });
  }

  handleClickLogin = (e: Event) => {
    //登入
    let account = this.state.account
    if (account === '') {
      return
    };
    let password = this.state.password      
    if (password === '') {
      return
    };

    let username = AppGlobal.getDefaultUsername()
    if (username === '') {
      //平板上帳號密碼的自動存取，有時讀取會無法進入onChange作用，導致抓不到值，所以必須自行存取預設
      //沒有預設帳號時，確認是否儲存帳號和密碼
      this.toggleSaveAlert()
    } else {
      this.PostLogin(account, password)
    };
  }

  textChangeForAccount = (e: Event) => {
    this.setState({account: e.target.value})
  }

  textChangeForPassword = (e: Event) => {
    this.setState({password: e.target.value})
  }

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
              <CardGroup>
                <Card className="p-4">
                  <CardBody className="text-center">
                    <Form>
                      <h1>登入</h1>
                      <p className="text-muted">請輸入帳號和密碼</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" value={this.state.account} onChange={this.textChangeForAccount} placeholder="請輸入帳號" required />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" value={this.state.password} onChange={this.textChangeForPassword} placeholder="請輸入密碼" required />
                      </InputGroup>
                      <Row className="justify-content-center">
                        <Col xs="6">
                          <Button color="primary" className="px-4" onClick={this.handleClickLogin}>登入</Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </CardGroup>
          </Row>
        </Container>
        <Modal isOpen={this.state.save_alert_modal} toggle={this.toggleSaveAlertNo} className={'modal-sm ' + this.props.className}>
          <ModalHeader toggle={this.toggleSaveAlertNo}>訊息</ModalHeader>
          <ModalBody>是否儲存帳號和密碼？</ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.toggleSaveAlertOK}>是</Button>{' '}
            <Button color="secondary" onClick={this.toggleSaveAlertNo}>否</Button>                      
          </ModalFooter>
        </Modal>
        <Modal isOpen={this.state.error_alert_modal} toggle={this.toggleErrorAlert} className={'modal-sm ' + this.props.className}>
          <ModalHeader toggle={this.toggleErrorAlert}>訊息</ModalHeader>
          <ModalBody>{this.state.error_message}</ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.toggleErrorAlert}>確定</Button>{' '}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default Login;
