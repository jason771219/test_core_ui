import React, { Component } from 'react';
import AppGlobal from '../../app-global.js'
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CCollapse,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CFade,
  CForm,
  CFormGroup,
  CFormText,
  CValidFeedback,
  CInvalidFeedback,
  CTextarea,
  CInput,
  CInputFile,
  CInputCheckbox,
  CInputRadio,
  CInputGroup,
  CInputGroupAppend,
  CInputGroupPrepend,
  CDropdown,
  CInputGroupText,
  CLabel,
  CSelect,
  CRow,
  CSwitch,
  CDataTable,
  CBadge,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle
} from '@coreui/react'
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';



import CIcon from '@coreui/icons-react'
import { DocsLink } from 'src/reusable'

const getBadge = status => {
  switch (status) {
    case 'Active': return 'success'
    case 'Inactive': return 'secondary'
    case 'Pending': return 'warning'
    case 'Banned': return 'danger'
    default: return 'primary'
  }
}

const fields = ['name','price']



class ExpenseForm extends Component {
  state = {
    name: '',
    price: '',
    expense_list: [],
    alert_modal: false,
    alert_message: ''
  };

  

  constructor(props) {
    super(props);

    let isLogin = AppGlobal.getIsLogin()
    if (!isLogin) {
      this.signOut()
      return
    };
    this.handleClickExpense = this.handleClickExpense.bind(this)
    this.toggleAlert = this.toggleAlert.bind(this);
    
    this.getExpenseList()  
  }  

  signOut() {
    // 登出
    const path = '/login';
    this.props.history.push(path);
  }

  async SubmitAddExpenseData() {
    try {
      let account = AppGlobal.getAccount()
      let token = AppGlobal.getToken()
      let password = AppGlobal.getDefaultPassword()
      if (account === '' || token === '') {
        this.signOut()
        return
      };

      console.log("account:",account);
      console.log("token:",token);

      const formData = new FormData()
      formData.append('name', this.state.name);
      formData.append('price', this.state.price);


      const headers = new Headers()
      // headers.append('Authorization', token)
      let base64EncodedString = window.btoa(account + ":" + password);
      headers.append('Authorization', 'Basic ' + base64EncodedString)

      let baseUrl = AppGlobal.getBaseUrl()
      const res = await fetch(baseUrl + '/expense/', {
        method: 'POST',
        headers: headers,
        body: formData
      });

      if (res.status >= 200 && res.status < 300) {
        const data = await res.json();     
        this.setState({
          name: '',
          price: '',
          alert_message: "Success",
          alert_modal: !this.state.alert_modal,
        });    
      } else {
        let error = new Error(res.statusText)
        error.response = res
        console.log(res);
      }
    } catch (e) {
        console.log(e);
    } finally {
        this.getExpenseList()
    }
  }  


  handleClickExpense() { 
    var price = this.state.price;
    if (isNaN(price)) {
      alert("Must input numbers");
      return;
    }
    if (this.state.name !== '') {
      this.SubmitAddExpenseData()
    };
  }  

  toggleAlert() {
    this.setState({
      alert_modal: !this.state.alert_modal,
    });
  }

  textChangeForName = (e: Event) => {
    this.setState({name: e.target.value})
  }

  textChangeForPrice = (e: Event) => {
    this.setState({price: e.target.value})
  }

  async getExpenseList() {
    try {
      let account = AppGlobal.getAccount()
      let token = AppGlobal.getToken()
      let password = AppGlobal.getDefaultPassword()
      if (account === '' || token === '') {
        this.signOut()
        return
      };

      const headers = new Headers()

      let base64EncodedString = window.btoa(account + ":" + password);
      headers.append('Authorization', 'Basic ' + base64EncodedString)


      let baseUrl = AppGlobal.getBaseUrl()
      const res = await fetch(baseUrl + '/expense/', {
        method: 'GET',
        headers: headers,
      });

      if (res.status >= 200 && res.status < 300) {
        const json_data = await res.json();
          let newArray = [];
          let results = json_data['results']
          for (var i = 0; i < results.length; i++) {
              let data_obj = results[i]
              var name = data_obj.name;
              var price = data_obj.price;
              let data_dict = {"name":name, "price":price}
              newArray.push(data_dict)
          }
          
          this.setState({expense_list:newArray}) 



      }

    }
    catch (e) {
      console.log(e);
    }  
    finally {
        
    } 

  } 




  render() {

    return (
    <>
      <CRow>
        <CCol xs="12" sm="6">
          <CCard>
            <CCardHeader>
              Add Expense
              <DocsLink name="-Input"/>
            </CCardHeader>
            <CCardBody>
              <CRow>
                <CCol xs="12">
                  <CFormGroup>
                    <CLabel htmlFor="name">Name</CLabel>
                    <CInput id="name" value={this.state.name} onChange={this.textChangeForName} placeholder="Enter name" required />
                  </CFormGroup>
                </CCol>
              </CRow>
              <CRow>
                <CCol xs="12">
                  <CFormGroup>
                    <CLabel htmlFor="ccnumber">Price</CLabel>
                    <CInput id="price" value={this.state.price} onChange={this.textChangeForPrice} placeholder="Enter price" required />
                  </CFormGroup>
                </CCol>
              </CRow>
                      
              <CCardFooter>
                <CButton type="submit" size="sm" color="primary" onClick={this.handleClickExpense}>AddItem</CButton>
              </CCardFooter>   

            </CCardBody>
                   
          </CCard>
        </CCol>
        
      </CRow>



        <CRow>
          <CCol xs="12" lg="12">
            <CCard>
              <CCardHeader>
                <i className="fa fa-align-justify"></i> Expense List
              </CCardHeader>
              <CCardBody>
              


                <CDataTable
                  items={this.state.expense_list}
                  fields={fields}
                  striped
                  itemsPerPage={5}
                  pagination
                  scopedSlots = {{
                    'status':
                      (item)=>(
                        <td>
                          <CBadge color="success">
                            {item.status}
                          </CBadge>
                        </td>
                      )

                  }}
                />           


              </CCardBody>
            </CCard>
          </CCol>
        </CRow>


        <div>
         <Modal isOpen={this.state.alert_modal} toggle={this.toggleAlert} className={'modal-sm ' + this.props.className}>
          <ModalHeader toggle={this.toggleAlert}>訊息</ModalHeader>
          <ModalBody>{this.state.alert_message}</ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.toggleAlert}>Sure</Button>{' '}
          </ModalFooter>
        </Modal>
        </div>
   
    </>
    );
  }


}

export default ExpenseForm
